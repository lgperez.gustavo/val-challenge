import {Injectable} from '@angular/core';
import {PostModel} from '../../models/timeline.model';
import {BehaviorSubject} from 'rxjs';
import {SelectedUser} from '../../models/employee.model';
import {EmployeeService} from './employee.service';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {
  private readonly timelineStorageKey = 'valispace_timeline';
  private historyPost: PostModel[];
  private userHistoryPost: PostModel[];
  private selectedUser: SelectedUser;
  private lastId: number;

  public historyPost$: BehaviorSubject<PostModel[]> = new BehaviorSubject<PostModel[]>(this.historyPost);
  public selectedUser$: BehaviorSubject<SelectedUser> = new BehaviorSubject<SelectedUser>(this.selectedUser);
  private userRegexMention: RegExp = /@\w+/;
  private userIdKey = 'userId=';

  constructor(private employeeService: EmployeeService) {
    this.historyPost = this.getPostsFromDb() || [];
    this.filterSelectedUserHistory();
    const maxIdNumber = Math.max(...this.historyPost.map(e => e.id));
    this.lastId = this.historyPost && this.historyPost.length ? maxIdNumber : 0;
  }

  public submitPost(post: PostModel): void {
    /* verify if has user mentioned to replace for userId: */
    post.text = this.checkForMention(post.text);
    /* If has id is editing mode */
    if (post.id) {
      const postIndex = this.historyPost.findIndex(hp => hp.id === post.id);
      if (postIndex !== -1) {
        this.historyPost[postIndex] = {...this.historyPost[postIndex], ...post};
      }
    } else {
      this.historyPost.push({...post, id: ++this.lastId});
    }
    this.save();
  }

  public updateSelectedUser(selectedUser: SelectedUser): void {
    this.selectedUser = selectedUser;
    this.selectedUser$.next(this.selectedUser);
    this.filterSelectedUserHistory();
  }

  public removePost(id: number): void {
    const postIndex = this.historyPost.findIndex(post => post.id === id);
    if (postIndex !== -1) {
      this.historyPost.splice(postIndex, 1);
      this.save();
    }
  }

  private getPostsFromDb(): PostModel[] {
    return JSON.parse(localStorage.getItem(this.timelineStorageKey));
  }

  private save(): void {
    this.historyPost = this.historyPost.sort((a: PostModel, b: PostModel) => (new Date(b.date).valueOf() - new Date(a.date).valueOf()));
    localStorage.setItem(this.timelineStorageKey, JSON.stringify(this.historyPost));
    this.filterSelectedUserHistory();
  }

  private filterSelectedUserHistory(): void {
    if (!this.selectedUser) {
      return;
    }
    /* Simple Treatment to display all posts */
    if (this.selectedUser.userId !== 0) {
      this.userHistoryPost = this.historyPost.filter(up => up.userId === this.selectedUser.userId);
      this.historyPost$.next(this.userHistoryPost);
    } else {
      this.historyPost$.next(this.historyPost);
    }
  }

  private replaceUserMention(text: string): string {
    const textSplited = text.split(' ');
    let returnText = '';
    textSplited.forEach(textSlice => {
      if (textSlice.substr(0, 1) === '@') {
        const userName = textSlice.match(this.userRegexMention)[0];
        const userId = this.getUserIdByUserName(userName);
        textSlice = textSlice.replace(this.userRegexMention, this.userIdKey + userId);
      }
      returnText += `${textSlice} `;
    });
    return returnText;
  }

  private getUserIdByUserName(userName): string | number {
    return this.employeeService.getUserIdByUserName(userName.substr(1));
  }

  private checkForMention(text: string): string {
    let returnText: string = text;
    if (returnText.indexOf('@') !== -1) {
      returnText = this.replaceUserMention(returnText);
    }
    return returnText;
  }
}
