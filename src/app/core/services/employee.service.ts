import {Injectable} from '@angular/core';
import {EmployeeModel} from '../../models/employee.model';
import {BehaviorSubject} from 'rxjs';
import {isNumber} from 'util';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private readonly employeeStorageKey = 'valispace_employees';
  private employees: EmployeeModel[];
  private lastId: number;
  private userIdKey = 'userId=';
  private regexUserKey: RegExp = /\w+=\d+/;
  private negatedRegexUserKey: RegExp = new RegExp(`[^${this.regexUserKey}]`);

  public employees$: BehaviorSubject<EmployeeModel[]> = new BehaviorSubject<EmployeeModel[]>(this.employees);

  constructor() {
    this.employees = this.getEmployeesFromDb() || [];
    this.employees$.next(this.employees);
    const maxIdNumber = Math.max(...this.employees.map(e => e.id));
    this.lastId = this.employees && this.employees.length ? maxIdNumber : 0;
  }

  public addEmployee(employee: EmployeeModel): void {
    this.employees.push({...employee, id: ++this.lastId});
  }

  public removeEmployee(employeeId: number): void {
    const removeIndex = this.employees.findIndex(emp => emp.id === employeeId);
    if (removeIndex !== -1) {
      this.employees.splice(removeIndex, 1);
      this.save();
    }
  }

  public submitEmployee(value: EmployeeModel): string {
    if (this.hasSamePhoneNumber(value)) {
      return 'this phone number is already in use, please inform another one or update your record';
    }
    const employeeIndex =  this.employees.findIndex(e => e.id === value.id);
    if (employeeIndex !== -1) {
      this.employees[employeeIndex] = value;
    } else {
      this.addEmployee(value);
    }
    this.save();
  }

  public getUserIdByUserName(userName: string): string | number {
    return this.employees.find(employee => employee.userName === userName).id;
  }

  public getUserNameById(userId: string): string {
    let userIdValue = userId;
    let hasDifferentCharacter = false;
    const bla = userIdValue.replace(this.userIdKey, '');
    if (isNaN(+bla)) {
      hasDifferentCharacter = true;
    }
    userIdValue = userId.match(this.regexUserKey)[0]; // this will prevent comma, dots and other characters at the end
    userIdValue = userIdValue.replace(this.userIdKey, '');
    if (!userIdValue) {
      return 'user not found';
    }
    const returnValue = this.employees.find(employee => employee.id === +userIdValue).userName;
    return hasDifferentCharacter ? returnValue + userId.match(this.negatedRegexUserKey)[0] : returnValue;
  }

  private save(): void {
    localStorage.setItem(this.employeeStorageKey, JSON.stringify(this.employees));
    this.employees$.next(this.employees);
  }

  private getEmployeesFromDb(): EmployeeModel[] {
    return JSON.parse(localStorage.getItem(this.employeeStorageKey));
  }

  private hasSamePhoneNumber(value: EmployeeModel): boolean {
    const filteredEmployees = this.employees.filter(employee => employee.id !== value.id);

    return filteredEmployees.some(emp => emp.phoneNumber === value.phoneNumber);
  }
}
