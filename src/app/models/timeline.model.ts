export interface PostModel {
  id?: number;
  date: Date;
  text: string;
  userId?: string | number;
  userName?: string;
}
