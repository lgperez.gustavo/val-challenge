export interface EmployeeModel {
  id?: number;
  userName: string;
  phoneNumber: string | number;
  role: string;
  name: string;
}

export interface DropdownOption {
  label: string;
  value: string;
  id: string | number;
}

export  interface SelectedUser {
  userName: string;
  userId: string | number;
}
