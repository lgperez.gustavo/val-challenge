import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeePageComponent} from './features/employees/employee-page/employee-page.component';
import {TimelinePageComponent} from './features/timeline/timeline-page/timeline-page.component';


const routes: Routes = [
  {
    path: 'admin',
    component: EmployeePageComponent
  },
  {
    path: '',
    component: TimelinePageComponent
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
