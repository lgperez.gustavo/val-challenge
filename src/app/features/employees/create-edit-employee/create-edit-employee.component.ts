import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EmployeeModel} from '../../../models/employee.model';
import {EmployeeService} from '../../../core/services/employee.service';

@Component({
  selector: 'app-create-edit-employee',
  templateUrl: './create-edit-employee.component.html',
  styleUrls: ['./create-edit-employee.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateEditEmployeeComponent implements OnInit {
  public errorMessage: string;

  @Input()
  public set editingEmployee(employee: EmployeeModel) {
    this.isEditing = !!employee;
    if (employee) {
      this.createEditUserForm.patchValue(employee);
    } else {
      this.createEditUserForm.reset();
    }
    this.cd.markForCheck();
  }

  @Output()
  public hideForm: EventEmitter<boolean> = new EventEmitter<boolean>();

  public isEditing = false;

  public createEditUserForm: FormGroup = new FormGroup({});

  constructor(private fb: FormBuilder, private employeeService: EmployeeService, private cd: ChangeDetectorRef) {
    this.buildForm();
  }

  ngOnInit() {
  }

  public get userName(): AbstractControl | null {
    return this.createEditUserForm.get('userName');
  }

  public get phone(): AbstractControl | null {
    return this.createEditUserForm.get('phoneNumber');
  }

  public get role(): AbstractControl | null {
    return this.createEditUserForm.get('role');
  }

  public get name(): AbstractControl | null {
    return this.createEditUserForm.get('name');
  }

  public submitEmployee(value: EmployeeModel): void {
    this.errorMessage = this.employeeService.submitEmployee(value);
    if (this.isEditing) {
      this.isEditing = false;
    }
    if (this.errorMessage) {
      this.cd.markForCheck();
      return;
    }
    this.hideForm.emit(true);
    this.createEditUserForm.reset();
  }

  public onCancelClick(): void {
    this.createEditUserForm.reset();
    this.hideForm.emit(true);
  }

  private buildForm(): void {
    this.createEditUserForm = this.fb.group({
      id: [null],
      userName: [null, Validators.compose([Validators.required, Validators.pattern(/[\w-_]+/)])],
      // simple way to validate the phone number
      phoneNumber: [null, Validators.compose([Validators.required, Validators.pattern(/\+?^.\d{7,14}$/)])],
      role: [null, Validators.required],
      name: [null, Validators.required]
    });
  }
}
