import {ChangeDetectionStrategy, Component, Output, OnInit, EventEmitter} from '@angular/core';
import {EmployeeModel} from '../../../models/employee.model';
import {EmployeeService} from '../../../core/services/employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeListComponent implements OnInit {

  @Output()
  public editingEmployee: EventEmitter<EmployeeModel | null> = new EventEmitter<EmployeeModel | null>();
  @Output()
  public showForm: EventEmitter<boolean> = new EventEmitter<boolean>();

  public selectedEmployee?: EmployeeModel;

  constructor(public employeeService: EmployeeService) { }

  ngOnInit() {}

  public trackByFn(index: number, employee: EmployeeModel): number {
    return employee.id || index;
  }

  public selectEmployee(employee: EmployeeModel): void {
    this.selectedEmployee = this.selectedEmployee && this.selectedEmployee.id === employee.id ? null : employee;
  }

  public deleteEmployee(): void {
    this.employeeService.removeEmployee(this.selectedEmployee.id);
    this.selectedEmployee = null;
  }

  public editEmployee(): void {
    this.editingEmployee.emit(this.selectedEmployee);
    this.showFormEmitter();
  }

  public addEmployee(): void {
    this.selectedEmployee = undefined;
    this.editingEmployee.emit(null);
    this.showFormEmitter();
  }

  private showFormEmitter(): void {
    this.showForm.emit(true);
  }
}
