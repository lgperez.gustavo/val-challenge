import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {EmployeeService} from '../../../core/services/employee.service';

@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeInfoComponent implements OnInit {

  @Input()
  public userId: string;

  public userName: string;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.userName = `@${this.employeeService.getUserNameById(this.userId)}`;
  }

}
