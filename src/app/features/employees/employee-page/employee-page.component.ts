import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {EmployeeModel} from '../../../models/employee.model';

@Component({
  selector: 'app-employees-page',
  templateUrl: './employee-page.component.html',
  styleUrls: ['./employee-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeePageComponent {

  public editingEmployee?: EmployeeModel;
  public showForm = false;

  constructor(private cd: ChangeDetectorRef) {
  }

  public onEditEmployee(employee: EmployeeModel): void {
    this.editingEmployee = employee;
    this.cd.markForCheck();
  }
}
