import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeePageComponent } from './employee-page/employee-page.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CreateEditEmployeeComponent } from './create-edit-employee/create-edit-employee.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';



@NgModule({
  declarations: [EmployeePageComponent, EmployeeListComponent, CreateEditEmployeeComponent, EmployeeInfoComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [EmployeePageComponent, EmployeeListComponent, EmployeeInfoComponent]
})
export class EmployeeModule { }
