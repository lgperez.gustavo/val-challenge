import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit } from '@angular/core';
import {DropdownOption, SelectedUser} from '../../../models/employee.model';
import {EmployeeService} from '../../../core/services/employee.service';
import {TimelineService} from '../../../core/services/timeline.service';

@Component({
  selector: 'app-user-selector',
  templateUrl: './user-selector.component.html',
  styleUrls: ['./user-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserSelectorComponent implements OnInit {

  public isDropdownOpen = false;
  public selectedOptionValue?: DropdownOption;
  public options: DropdownOption[] = [];

  constructor(private cd: ChangeDetectorRef, private employeeService: EmployeeService, private timelineService: TimelineService) { }

  @HostListener('document:scroll')
  public onScroll(): void {
    if (this.isDropdownOpen) {
      this.isDropdownOpen = false;
    }
  }

  @HostListener('document:click')
  public closesDropDown(): void {
    if (this.isDropdownOpen) {
      this.isDropdownOpen = false;
      this.cd.markForCheck();
    }
  }

  public ngOnInit(): void {
    this.employeeService.employees$.pipe().subscribe(employees => {
      this.options = [];
      employees.forEach(employee => {
        const employeeOption: DropdownOption = {
          label: employee.name,
          value: employee.userName,
          id: employee.id
        };
        this.options.push(employeeOption);
      });
      this.changeSelectedOption(this.options[0].value);
      this.cd.markForCheck();
    });
  }

  public toggleDropdown(e: MouseEvent): void {
    this.isDropdownOpen = !this.isDropdownOpen;
    this.cd.detectChanges();
    e.stopPropagation();
  }

  public trackByFn(index: number, option: DropdownOption) {
    return option && option.id ? option.id : index;
  }

  public changeSelectedOption(optionValue: string) {
    const option = this.options.find(optionV => optionV.value === optionValue);
    const selectedUser: SelectedUser = {
      userId: option.id,
      userName: option.value
    };
    this.timelineService.updateSelectedUser(selectedUser);
    this.selectedOptionValue = option;
    this.cd.markForCheck();
  }
}
