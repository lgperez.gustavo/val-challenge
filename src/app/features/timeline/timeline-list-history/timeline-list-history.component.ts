import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TimelineService} from '../../../core/services/timeline.service';
import {PostModel} from '../../../models/timeline.model';
import {take} from 'rxjs/operators';
import {MentionConfig} from 'angular-mentions';
import {EmployeeService} from '../../../core/services/employee.service';

@Component({
  selector: 'app-timeline-list-history',
  templateUrl: './timeline-list-history.component.html',
  styleUrls: ['./timeline-list-history.component.scss']
})
export class TimelineListHistoryComponent implements OnInit {

  public editingPostId: number;
  public currentText: string;
  public mentionConfig: MentionConfig;

  constructor(public timelineService: TimelineService, private cd: ChangeDetectorRef, private employeeService: EmployeeService) { }

  public ngOnInit(): void {
    this.employeeService.employees$.pipe(take(1)).subscribe(emp => {
      this.mentionConfig = {
        mentions: [
          {
            items: emp.map(e => e.userName),
            triggerChar: '@'
          },
          {
            items: emp.map(e => e.phoneNumber),
            triggerChar: '#'
          }
        ]
      };
    });
  }

  public trackByFn(index: number, historyLine: PostModel): number {
    return historyLine.id || index;
  }

  public removePost(id: number): void {
    this.timelineService.removePost(id);
  }

  public editPost(id: number, currentText: string): void {
    this.editingPostId = id;
    this.currentText = currentText;
    this.cd.markForCheck();
  }

  public savePost(): void {
    this.timelineService.submitPost({
      date: new Date(Date.now()),
      text: this.currentText,
      id: this.editingPostId
    });
    this.editingPostId = null;
    this.cd.markForCheck();
  }

  public cancel(): void {
    this.editingPostId = null;
    this.cd.markForCheck();
  }
}
