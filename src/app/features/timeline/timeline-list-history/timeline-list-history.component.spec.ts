import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineListHistoryComponent } from './timeline-list-history.component';

describe('TimelineListHistoryComponent', () => {
  let component: TimelineListHistoryComponent;
  let fixture: ComponentFixture<TimelineListHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineListHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineListHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
