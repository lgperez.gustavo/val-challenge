import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelinePageComponent } from './timeline-page/timeline-page.component';
import {FormsModule} from '@angular/forms';
import { TimelineListHistoryComponent } from './timeline-list-history/timeline-list-history.component';
import { UserSelectorComponent } from './user-selector/user-selector.component';
import {MentionModule} from 'angular-mentions';
import {EmployeeModule} from '../employees/employee.module';



@NgModule({
  declarations: [TimelinePageComponent, TimelineListHistoryComponent, UserSelectorComponent],
    imports: [
        CommonModule,
        FormsModule,
        MentionModule,
        EmployeeModule
    ],
  exports: [TimelinePageComponent, UserSelectorComponent, TimelineListHistoryComponent, MentionModule]
})
export class TimelineModule { }
