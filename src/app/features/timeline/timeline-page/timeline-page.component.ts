import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {TimelineService} from '../../../core/services/timeline.service';
import {SelectedUser} from '../../../models/employee.model';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';
import {EmployeeService} from '../../../core/services/employee.service';
import {MentionConfig} from 'angular-mentions';

@Component({
  selector: 'app-timeline-page',
  templateUrl: './timeline-page.component.html',
  styleUrls: ['./timeline-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelinePageComponent implements OnInit, OnDestroy {
  private readonly unsubscribe = new Subject<void>();

  private selectedUser: SelectedUser;

  public inputText: string;
  public mentionConfig: MentionConfig;

  constructor(private timelineService: TimelineService, private cd: ChangeDetectorRef, private employeeService: EmployeeService) { }

  public ngOnInit(): void {
    this.timelineService.selectedUser$.pipe(takeUntil(this.unsubscribe)).subscribe(selectedUser => {
        this.selectedUser = selectedUser;
        this.cd.markForCheck();
      }
    );

    this.employeeService.employees$.pipe(take(1)).subscribe(emp => {
      this.mentionConfig = {
        mentions: [
          {
            items: emp.map(e => e.userName),
            triggerChar: '@'
          },
          {
            items: emp.map(e => e.phoneNumber),
            triggerChar: '#'
          }
        ]
      };
    });
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public savePost(): void {
    if (!this.inputText) {
      return;
    }
    this.timelineService.submitPost({
      date: new Date(Date.now()),
      text: this.inputText,
      userId: this.selectedUser.userId,
      userName: this.selectedUser.userName
    });
    this.inputText = '';
    this.cd.markForCheck();
  }
}
