import {ChangeDetectionStrategy, Component} from '@angular/core';
import {LinkModel} from '../../../models/structure.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {

  public readonly valispaceUrl = 'https://www.valispace.com/';
  public links: LinkModel[] = [];
  constructor() {
    /* define the links but can be used by a service */
    this.links = [
      {url: '/admin', name: 'Employees'},
      {url: '/', name: 'Timeline'}
    ];
  }

  public trackByFn(index: number): number {
    return index;
  }
}
